#pragma once

__global__ void initializationKernel(
        short x0, short x1, short *x,
        float * t, int numTrajectories, int numIters);

__global__ void dimerizationKernel(
        int pass, const float *u,
        short *x, float *t, int *iters, char *isSampleDone,
        float endTime, int omega, int numIters, int numSamples);

__global__ void reduceIsDoneKernel(const char *isSampleDone, int *blocksDoneCount, int numSamples);

__device__ short atomicAddShort(short* address, short val);

__global__ void binningKernel(
        const float *u, short *x, float *t, int *iters,
        int numIters, int numSamples, int dtBin, int numBins,
        short *trajSaL, short *trajSbL, int *ntrajL,
        short *trajSa, short *trajSb, int *ntraj);

__global__ void averageKernel(const short *trajSaDev, const short *trajSbDev, const int *ntrajDev,
                            double *trajSaAvgDev, double *trajSbAvgDev, int numBins);
