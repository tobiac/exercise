#include "kernels.h"

/// Initialize reaction states.
__global__ void initializationKernel(
        short Sa, short Sb, short *x,
        float *t, int numSamples, int numIters)
{
    const int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= numSamples)
        return;

    // Every sample starts with (Sa, Sb) at t=0.
    t[idx] = 0.f;
    x[0 * numIters * numSamples + idx] = Sa;
    x[1 * numIters * numSamples + idx] = Sb;
}


/// Reaction simulation. This kernel uses precomputed random uniform samples
/// (from 0 to 1) to compute up to `numIters` steps of the SSA algorithm. The
/// values of Sa and Sb are stored in `x`, the time values in `t`. Buffer
/// `iters` stores the number of performed iterations, and `isSampleDone`
/// whether or not the sample has reach the final state (t >= endTime).
__global__ void dimerizationKernel(
        int pass, const float *u,
        short *x, float *t, int *iters, char *isSampleDone,
        float endTime, int omega, int numIters, int numSamples)
{
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int rngOffset = blockIdx.x * blockDim.x * 2 * numIters + threadIdx.x;

    if (idx >= numSamples)
        return;

    // Reaction rates.
    const float k1 = 1;
    const float k2 = 1;
    const float k3 = 0.2f / omega;
    const float k4 = 20.f * omega;

    // State variables.
    float time;
    float Sa, Sb;

    // Load state.
    const bool continuation = pass > 0 && !isSampleDone[idx];
    if (continuation) {
        Sa   = x[0 * numIters * numSamples + (numIters - 1) * numSamples + idx];
        Sb   = x[1 * numIters * numSamples + (numIters - 1) * numSamples + idx];
        time = t[(numIters - 1) * numSamples + idx];
    } else {
        Sa   = x[0 * numIters * numSamples + idx];
        Sb   = x[1 * numIters * numSamples + idx];
        time = t[idx];
    }

    // Simulation loop.
    int iter;
    for (iter = 0; time < endTime && iter < numIters && (pass == 0 || !isSampleDone[idx]); ++iter) {
        // Accumulated propensities.
        const float a1 = k1*Sa;
        const float a2 = a1 + k2*Sb;
        const float a3 = a2 + k3*Sa*Sb;
        const float a4 = a3 + k4;
        const float a0 = a4;

        time -= 1 / a0 * log(u[rngOffset]);
        rngOffset += blockDim.x;

        const float beta = a0 * u[rngOffset];
        rngOffset += blockDim.x;

        const int d1 = (beta < a1);
        const int d2 = (beta >= a1 && beta < a2);
        const int d3 = (beta >= a2 && beta < a3);
        const int d4 = (beta >= a3);

        Sa += -d1 + d3;
        Sb += -d2 - d3 + d4;

        t[iter * numSamples + idx] = time;
        x[0 * numIters * numSamples + iter * numSamples + idx] = Sa;
        x[1 * numIters * numSamples + iter * numSamples + idx] = Sb;
    }

    // Termination markers.
    iters[idx]        = iter;
    isSampleDone[idx] = time >= endTime || isSampleDone[idx];
}

/// Returns the sum of all values `a` within a warp,
///with the correct answer returned only by the 0th thread of a warp.
__device__ double sumWarp(double a) {
    for(int i = 1; i < 32; i<<=1){
        a += __shfl_down_sync(0xffffffff, a, i, warpSize);
    }
    return a;
}

/// Returns the sum of all values `a` within a block,
/// with the correct answer returned only by the 0th thread of a block.
__device__ double sumBlock(double a) {
    // Sum within each warp
    a = sumWarp(a);
    int warpId = threadIdx.x >> 5;
    __syncthreads();

    __shared__ double s[32];
    if((threadIdx.x & 31) == 0){
        s[warpId] = a;
    }

    __syncthreads();
    if(threadIdx.x < warpSize){
        a = sumWarp(s[threadIdx.x]);
    }

    return a;
}

/// Store the sum of the subarray isSampleDone[1024*b : 1024*b+1023] in blocksDoneCount[b].
__global__ void reduceIsDoneKernel(const char *isSampleDone, int *blocksDoneCount, int numSamples) {
    // TODO: Implement the reduction that computes how many samples in a block have completed.
    //       isSampleDone[sampleIdx] = 1 if sample has finished, 0 if not.
    //       blocksDoneCount[blockIdx] = 0..threads-1 (the value to compute).
    //       Feel free to reuse the code from Q1.
    // Using blockSum from Q1:
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int tmpSum = sumBlock((idx < numSamples) ? isSampleDone[idx] : 0);
    if(threadIdx.x == 0){
        blocksDoneCount[blockIdx.x] = tmpSum;
    }
}

// Kernel to add a short int to address
__device__ short atomicAddShort(short* address, short val){
    unsigned int *base_address = (unsigned int *) ((char *)address - ((size_t)address & 2));
    unsigned int long_val = ((size_t)address & 2) ? ((unsigned int)val << 16) : (unsigned short)val;
    unsigned int long_old = atomicAdd(base_address, long_val);

    if((size_t)address & 2) {
        return (short)(long_old >> 16);
    } else {
        unsigned int overflow = ((long_old & 0xffff) + long_val) & 0xffff0000;
        if (overflow)
            atomicSub(base_address, overflow);
        return (short)(long_old & 0xffff);
    }
}

// TODO: Implement the binning mechanism.
//
//       Add function prototypes to src/kernels.h, such that ssa.cu can access them.
__global__ void binningKernel(
        const float *u, short *x, float *t, int *iters,
        int numIters, int numSamples, int dtBin, int numBins,
        short *trajSaL, short *trajSbL, int *ntrajL,
        short *trajSa, short *trajSb, int *ntraj){

    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    double time;
    int k;
    for (int iter = 0; iters[idx]; ++iter) {
        // Current timestep
        time = t[iter * numSamples + idx];
        // Compute to which bin the sample belongs to
        k = static_cast<int>(time / dtBin);
        // Collect the samples
        trajSaL[k + idx * numBins] += x[0 * numIters * numSamples + iter * numSamples + idx];
        trajSbL[k + idx * numBins] += x[1 * numIters * numSamples + iter * numSamples + idx];
        ++ntrajL[k + idx * numBins];
    }

    // Sum the trajectories together
    short tmpSa;
    short tmpSb;
    int tmpN;
    for(int i = 0; i < numBins; ++i){
        tmpSa = sumBlock(trajSaL[i + idx * numBins]);
        tmpSb = sumBlock(trajSbL[i + idx * numBins]);
        tmpN = sumBlock(ntrajL[i + idx * numBins]);
        if(threadIdx.x == 0){
            atomicAddShort(&trajSa[i], tmpSa);
            atomicAddShort(&trajSb[i], tmpSb);
            atomicAdd(&ntraj[i], tmpN);
        }
    }
}

__global__ void averageKernel(const short *trajSaDev, const short *trajSbDev, const int *ntrajDev,
                            double *trajSaAvgDev, double *trajSbAvgDev, int numBins){
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if(idx < numBins){
        trajSaAvgDev[idx] = static_cast<double>(trajSaDev[idx] / double(ntrajDev[idx]));
        trajSbAvgDev[idx] = static_cast<double>(trajSbDev[idx] / double(ntrajDev[idx]));
    }
}
