// Use
//     cuobjdump -sass alignment
// to see the difference between compiled functions.

#include <cuda_runtime.h>

__global__ void copy4x1(const int *from, int * __restrict__ to) {
    to[0] = from[0];
    to[1] = from[1];
    to[2] = from[2];
    to[3] = from[3];
}

__global__ void copy2x2(const int2 *from, int2 * __restrict__ to) {
    to[0] = from[0];
    to[1] = from[1];
}

__global__ void copy1x4(const int4 *from, int4 * __restrict__ to) {
    to[0] = from[0];
}

int main() {
}
