#include "utils.h"


__global__ void dummyKernel() {
}

__global__ void dummyKernel2(double x) {
    double x0 = 1.0;
    double x1 = x0 * x;
    double x2 = x1 * x;
    double x3 = x2 * x;
    double x4 = x3 * x;
    double x5 = x4 * x;
    double x6 = x5 * x;
    double x7 = x6 * x;
    double x8 = x7 * x;
    double x9 = x8 * x;
    double x10 = x9 * x;
    double y = 0.0;
    y += x10;
    y += x9;
    y += x8;
    y += x7;
    y += x6;
    y += x5;
    y += x4;
    y += x3;
    y += x2;
    y += x1;
    if (y == 0.1)
        printf("Hello x=%f\n", x);
}

void benchmarkLaunch() {
    for (int threads = 128; threads <= 1024; threads += 128) {
        for (int i = 0; i < 3; ++i) {
            auto t0 = std::chrono::steady_clock::now();
            dummyKernel<<<1, threads>>>();
            auto t1 = std::chrono::steady_clock::now();
            double ns = (double)std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count();
            printf("threads=%d  dt=%.1f us\n", threads, ns / 1e3);
        }
    }

    for (int threads = 128; threads <= 1024; threads += 128) {
        auto t0 = std::chrono::steady_clock::now();
        dummyKernel2<<<1, threads>>>(1.0);
        auto t1 = std::chrono::steady_clock::now();
        double dtFirst = 1e-9 * (double)std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count();

        double dtLater = benchmark(1, [threads]() {
            // dummyKernel2<<<1024, threads>>>(1.123);
            CUDA_LAUNCH(dummyKernel2, 1024, threads,
                        1.123);
        });
        printf("threads=%4d    first=%4.1f us    avg=%4.1f us    Meval/s=%6.1f\n",
               threads, 1e6 * dtFirst, 1e6 * dtLater, 1e-6 * threads / dtLater);
    }
}

int main() {
    benchmarkLaunch();
}
