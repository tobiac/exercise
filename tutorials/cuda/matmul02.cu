// Alignment issue. Use [iy * M + x] instead of [iy * N + x],
// where M = (N + 31) / 32 * 32.
#include "utils.h"

using real = double;


__global__ void matMulKernel(int N, int M, const real *a, const real *b, real *c) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;

    if (ix >= N || iy >= N)
        return;

    real sum = 0;
    for (int k = 0; k < N; ++k)
        sum += a[iy * M + k] * b[k * M + ix];
    c[iy * N + ix] = sum;
}

// c = a * b
void matMulCPU(int N, int M, const real *a, const real *b, real *c) {
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        real sum = 0;
        for (int k = 0; k < N; ++k)
            sum += a[iy * M + k] * b[k * M + ix];
        c[iy * N + ix] = sum;
    }
}

void benchmarkMatrixMultiplication(int N, int M, bool check) {
    real *aHost;
    real *bHost;
    real *cHost;
    real *aDev;
    real *bDev;
    real *cDev;

    // Allocate.
    CUDA_CHECK(cudaMallocHost(&aHost, N * M * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&bHost, N * M * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&cHost, N * M * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&aDev, N * M * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&bDev, N * M * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&cDev, N * M * sizeof(real)));

    // Prepare A and B.
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        aHost[iy * M + ix] = iy + ix;
        bHost[iy * M + ix] = ix * ix + iy;
    }
    CUDA_CHECK(cudaMemcpy(aDev, aHost, N * M * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(bDev, bHost, N * M * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemset(cDev, 0, N * M * sizeof(real)));

    // Compute C = A * B on GPU.
    double dt = benchmark(10, [N, M, aDev, bDev, cDev]() {
        dim3 threads(32, 32, 1);
        dim3 blocks((N + threads.x - 1) / threads.x,
                    (N + threads.y - 1) / threads.y,
                    1);
        matMulKernel<<<blocks, threads>>>(N, M, aDev, bDev, cDev);
    });
    double gflops = 1e-9 * 2LL * N * N * N / dt;
    printf("N=%d  M=%d  GFLOP/s=%.1f\n", N, M, gflops);

    // Check correctnes.
    if (check) {
        matMulCPU(N, M, aHost, bHost, cHost);
        double *tmpHost = aHost;
        CUDA_CHECK(cudaMemcpy(tmpHost, cDev, N * M * sizeof(real), cudaMemcpyDeviceToHost));
        for (int iy = 0; iy < N; ++iy)
        for (int ix = 0; ix < N; ++ix) {
            if (tmpHost[iy * M + ix] != cHost[iy * M + ix]) {
                fprintf(stderr, "Incorrect result at [%d][%d] --> host=%f gpu=%f\n",
                        iy, ix, cHost[iy * M + ix], tmpHost[iy * M + ix]);
                exit(1);
            }
        }
        // printf("GPU result correct.\n");
    }

    // Deallocate.
    CUDA_CHECK(cudaFree(cDev));
    CUDA_CHECK(cudaFree(bDev));
    CUDA_CHECK(cudaFree(aDev));
    CUDA_CHECK(cudaFreeHost(cHost));
    CUDA_CHECK(cudaFreeHost(bHost));
    CUDA_CHECK(cudaFreeHost(aHost));
}


int main() {
    benchmarkMatrixMultiplication(253, 253, true);
    benchmarkMatrixMultiplication(253, 256, true);
    benchmarkMatrixMultiplication(3069, 3069, false);
    benchmarkMatrixMultiplication(3069, 3072, false);
}
