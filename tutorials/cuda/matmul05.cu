// Shared memory (no ILP optimization).
#include "utils.h"

using real = double;

constexpr int BLOCK_SIZE = 32;

__global__ void matMulKernel(int N, const real *a, const real *b, real *c) {
    int ix = blockIdx.x * blockDim.x + threadIdx.x;
    int iy = blockIdx.y * blockDim.y + threadIdx.y;

    __shared__ real A[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ real B[BLOCK_SIZE][BLOCK_SIZE];

    real sum = 0;

    // Original code.
    // for (int k = 0; k < N; ++k)
    //     sum += a[iy * N + k] * b[k * N + ix];

    // "Blocking" without shared memory.
    // for (int k0 = 0; k0 < N; k0 += BLOCK_SIZE) {
    //     for (int k = 0; k < BLOCK_SIZE; ++k)
    //         sum += a[iy * N + (k + k0)] * b[(k0 + k) * N + ix];
    //     __syncthreads();
    // }

    for (int k0 = 0; k0 < N; k0 += BLOCK_SIZE) {
        A[threadIdx.y][threadIdx.x] = iy < N && k0 + threadIdx.x < N ? a[iy * N + k0 + threadIdx.x]   : 0;
        B[threadIdx.y][threadIdx.x] = ix < N && k0 + threadIdx.y < N ? b[(k0 + threadIdx.y) * N + ix] : 0;

		__syncthreads();
        for (int k = 0; k < BLOCK_SIZE; ++k)
            sum += A[threadIdx.y][k] * B[k][threadIdx.x];
		__syncthreads();
    }

    if (iy < N && ix < N)
        c[iy * N + ix] = sum;
}

// c = a * b
void matMulCPU(int N, const real *a, const real *b, real *c) {
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        real sum = 0;
        for (int k = 0; k < N; ++k)
            sum += a[iy * N + k] * b[k * N + ix];
        c[iy * N + ix] = sum;
    }
}

void benchmarkMatrixMultiplication(int N, bool check) {
    real *aHost;
    real *bHost;
    real *cHost;
    real *aDev;
    real *bDev;
    real *cDev;

    // Allocate.
    CUDA_CHECK(cudaMallocHost(&aHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&bHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMallocHost(&cHost, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&aDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&bDev, N * N * sizeof(real)));
    CUDA_CHECK(cudaMalloc(&cDev, N * N * sizeof(real)));

    // Prepare A and B.
    for (int iy = 0; iy < N; ++iy)
    for (int ix = 0; ix < N; ++ix) {
        aHost[iy * N + ix] = iy + ix;
        bHost[iy * N + ix] = ix * ix + iy;
    }
    CUDA_CHECK(cudaMemcpy(aDev, aHost, N * N * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(bDev, bHost, N * N * sizeof(real), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemset(cDev, 0, N * N * sizeof(real)));

    // Compute C = A * B on GPU.
    double dt = benchmark(10, [N, aDev, bDev, cDev]() {
        dim3 threads(BLOCK_SIZE, BLOCK_SIZE, 1);
        dim3 blocks((N + threads.x - 1) / threads.x,
                    (N + threads.y - 1) / threads.y,
                    1);
        matMulKernel<<<blocks, threads>>>(N, aDev, bDev, cDev);
    });
    double gflops = 1e-9 * 2LL * N * N * N / dt;
    printf("N=%d   GFLOP/s=%.1f\n", N, gflops);

    // Check correctnes.
    if (check) {
        matMulCPU(N, aHost, bHost, cHost);
        double *tmpHost = aHost;
        CUDA_CHECK(cudaMemcpy(tmpHost, cDev, N * N * sizeof(real), cudaMemcpyDeviceToHost));
        for (int iy = 0; iy < N; ++iy)
        for (int ix = 0; ix < N; ++ix) {
            if (tmpHost[iy * N + ix] != cHost[iy * N + ix]) {
                fprintf(stderr, "Incorrect result at [%d][%d] --> host=%f gpu=%f\n",
                        iy, ix, cHost[iy * N + ix], tmpHost[iy * N + ix]);
                exit(1);
            }
        }
        // printf("GPU result correct.\n");
    }

    // Deallocate.
    CUDA_CHECK(cudaFree(cDev));
    CUDA_CHECK(cudaFree(bDev));
    CUDA_CHECK(cudaFree(aDev));
    CUDA_CHECK(cudaFreeHost(cHost));
    CUDA_CHECK(cudaFreeHost(bHost));
    CUDA_CHECK(cudaFreeHost(aHost));
}


int main() {
    benchmarkMatrixMultiplication(256, true);
    benchmarkMatrixMultiplication(259, true);
    benchmarkMatrixMultiplication(3072, false);
}
