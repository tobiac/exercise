#include <cuda_runtime.h>
#include <stdio.h>

__global__ void computeForcesKernel(int N, const double3 *p, double3 *f) {
    // TODO: Copy the code from `nbody_c.cu` and utilize shared memory.
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int tid = threadIdx.x;
    if (idx >= N){
        return;
	}

	extern __shared__ double3 sPos[];

    // TODO: Copy the code from `nbody_b.cu` and utilize rsqrt.
	double3 ftmp = make_double3(0.0, 0.0, 0.0);
	double3 particle_i = p[idx];
	for(int base = 0; base < N; base += blockDim.x){
		// Initialize shared memory
		sPos[tid] = p[base + tid];
		__syncthreads();

		    for (int i = 0; i < blockDim.x; ++i) {
				double dx = sPos[i].x - particle_i.x;
				double dy = sPos[i].y - particle_i.y;
				double dz = sPos[i].z - particle_i.z;
				// Instead of skipping the i == idx case, add 1e-150 to avoid division
				// by zero. (dx * inv_r will be exactly 0.0)
				double inv_r = rsqrt(1e-150 + dx * dx + dy * dy + dz * dz);
				double inv_r3 = inv_r*inv_r*inv_r;
				ftmp.x += dx * inv_r3;
				ftmp.y += dy * inv_r3;
				ftmp.z += dz * inv_r3;
		    }
		__syncthreads();
	}	
	f[idx].x += ftmp.x;
	f[idx].y += ftmp.y;
	f[idx].z += ftmp.z;
}

void computeForces(int N, const double3 *p, double3 *f) {
    constexpr int numThreads = 1024;
    int numBlocks = (N + numThreads - 1) / numThreads;

	size_t shmSize = numThreads * sizeof(double3);
    computeForcesKernel<<<numBlocks, numThreads, shmSize>>>(N, p, f);
}

