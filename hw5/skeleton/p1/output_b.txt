upload K=      17 -->  0.02 GB/s
upload K=      65 -->  0.07 GB/s
upload K=     251 -->  0.28 GB/s
upload K=    1001 -->  0.99 GB/s
upload K=    2001 -->  1.70 GB/s
upload K=    5001 -->  3.84 GB/s
upload K=   10001 -->  5.96 GB/s
upload K=   25001 -->  8.59 GB/s
upload K=   50001 --> 10.20 GB/s
upload K=  100001 --> 11.21 GB/s
upload K=  250001 --> 11.81 GB/s
upload K=  500001 --> 12.13 GB/s
upload K= 1000001 --> 12.29 GB/s
upload K= 5000001 --> 12.44 GB/s
upload K=20000001 --> 12.46 GB/s
upload K=50000001 --> 12.47 GB/s
Case p[i]=i  -->  K=      17  [a=b_p]   0.04 GB/s  [a_p=b]   0.04 GB/s written
Case p[i]=i  -->  K=      65  [a=b_p]   0.15 GB/s  [a_p=b]   0.14 GB/s written
Case p[i]=i  -->  K=     251  [a=b_p]   0.56 GB/s  [a_p=b]   0.56 GB/s written
Case p[i]=i  -->  K=    1001  [a=b_p]   2.22 GB/s  [a_p=b]   2.22 GB/s written
Case p[i]=i  -->  K=    2001  [a=b_p]   4.46 GB/s  [a_p=b]   4.46 GB/s written
Case p[i]=i  -->  K=    5001  [a=b_p]  11.03 GB/s  [a_p=b]  11.07 GB/s written
Case p[i]=i  -->  K=   10001  [a=b_p]  21.87 GB/s  [a_p=b]  21.99 GB/s written
Case p[i]=i  -->  K=   25001  [a=b_p]  52.47 GB/s  [a_p=b]  52.51 GB/s written
Case p[i]=i  -->  K=   50001  [a=b_p] 103.08 GB/s  [a_p=b] 100.84 GB/s written
Case p[i]=i  -->  K=  100001  [a=b_p] 177.32 GB/s  [a_p=b] 177.49 GB/s written
Case p[i]=i  -->  K=  250001  [a=b_p] 178.63 GB/s  [a_p=b] 188.90 GB/s written
Case p[i]=i  -->  K=  500001  [a=b_p] 188.72 GB/s  [a_p=b] 193.42 GB/s written
Case p[i]=i  -->  K= 1000001  [a=b_p] 146.62 GB/s  [a_p=b] 153.21 GB/s written
Case p[i]=i  -->  K= 5000001  [a=b_p]  33.91 GB/s  [a_p=b]  34.61 GB/s written
Case p[i]=i  -->  K=20000001  [a=b_p]  13.69 GB/s  [a_p=b]  14.02 GB/s written
Case p[i]=i  -->  K=50000001  [a=b_p]  10.32 GB/s  [a_p=b]  10.40 GB/s written
Case p[i]=(2*i)%K  -->  K=      17  [a=b_p]   0.04 GB/s  [a_p=b]   0.04 GB/s written
Case p[i]=(2*i)%K  -->  K=      65  [a=b_p]   0.14 GB/s  [a_p=b]   0.14 GB/s written
Case p[i]=(2*i)%K  -->  K=     251  [a=b_p]   0.56 GB/s  [a_p=b]   0.56 GB/s written
Case p[i]=(2*i)%K  -->  K=    1001  [a=b_p]   2.23 GB/s  [a_p=b]   2.23 GB/s written
Case p[i]=(2*i)%K  -->  K=    2001  [a=b_p]   4.43 GB/s  [a_p=b]   4.43 GB/s written
Case p[i]=(2*i)%K  -->  K=    5001  [a=b_p]  11.03 GB/s  [a_p=b]  11.01 GB/s written
Case p[i]=(2*i)%K  -->  K=   10001  [a=b_p]  21.86 GB/s  [a_p=b]  21.77 GB/s written
Case p[i]=(2*i)%K  -->  K=   25001  [a=b_p]  51.72 GB/s  [a_p=b]  51.92 GB/s written
Case p[i]=(2*i)%K  -->  K=   50001  [a=b_p]  97.34 GB/s  [a_p=b]  95.25 GB/s written
Case p[i]=(2*i)%K  -->  K=  100001  [a=b_p] 165.80 GB/s  [a_p=b] 155.64 GB/s written
Case p[i]=(2*i)%K  -->  K=  250001  [a=b_p] 196.36 GB/s  [a_p=b] 172.89 GB/s written
Case p[i]=(2*i)%K  -->  K=  500001  [a=b_p] 138.46 GB/s  [a_p=b]  97.27 GB/s written
Case p[i]=(2*i)%K  -->  K= 1000001  [a=b_p]  91.60 GB/s  [a_p=b]  74.48 GB/s written
Case p[i]=(2*i)%K  -->  K= 5000001  [a=b_p]  31.85 GB/s  [a_p=b]  25.48 GB/s written
Case p[i]=(2*i)%K  -->  K=20000001  [a=b_p]  13.51 GB/s  [a_p=b]  12.79 GB/s written
Case p[i]=(2*i)%K  -->  K=50000001  [a=b_p]  10.12 GB/s  [a_p=b]   9.96 GB/s written
Case p[i]=(4*i)%K  -->  K=      17  [a=b_p]   0.04 GB/s  [a_p=b]   0.04 GB/s written
Case p[i]=(4*i)%K  -->  K=      65  [a=b_p]   0.14 GB/s  [a_p=b]   0.14 GB/s written
Case p[i]=(4*i)%K  -->  K=     251  [a=b_p]   0.56 GB/s  [a_p=b]   0.56 GB/s written
Case p[i]=(4*i)%K  -->  K=    1001  [a=b_p]   2.22 GB/s  [a_p=b]   2.20 GB/s written
Case p[i]=(4*i)%K  -->  K=    2001  [a=b_p]   4.43 GB/s  [a_p=b]   4.37 GB/s written
Case p[i]=(4*i)%K  -->  K=    5001  [a=b_p]  11.00 GB/s  [a_p=b]  10.65 GB/s written
Case p[i]=(4*i)%K  -->  K=   10001  [a=b_p]  21.62 GB/s  [a_p=b]  21.12 GB/s written
Case p[i]=(4*i)%K  -->  K=   25001  [a=b_p]  51.42 GB/s  [a_p=b]  51.15 GB/s written
Case p[i]=(4*i)%K  -->  K=   50001  [a=b_p]  93.01 GB/s  [a_p=b]  85.92 GB/s written
Case p[i]=(4*i)%K  -->  K=  100001  [a=b_p] 139.40 GB/s  [a_p=b] 115.78 GB/s written
Case p[i]=(4*i)%K  -->  K=  250001  [a=b_p] 174.55 GB/s  [a_p=b] 125.27 GB/s written
Case p[i]=(4*i)%K  -->  K=  500001  [a=b_p] 102.88 GB/s  [a_p=b]  69.86 GB/s written
Case p[i]=(4*i)%K  -->  K= 1000001  [a=b_p]  64.67 GB/s  [a_p=b]  52.48 GB/s written
Case p[i]=(4*i)%K  -->  K= 5000001  [a=b_p]  26.76 GB/s  [a_p=b]  17.80 GB/s written
Case p[i]=(4*i)%K  -->  K=20000001  [a=b_p]  12.99 GB/s  [a_p=b]  11.75 GB/s written
Case p[i]=(4*i)%K  -->  K=50000001  [a=b_p]   9.98 GB/s  [a_p=b]   9.72 GB/s written
Case p[i]=i, 32-shuffled  -->  K=      17  [a=b_p]   0.04 GB/s  [a_p=b]   0.04 GB/s written
Case p[i]=i, 32-shuffled  -->  K=      65  [a=b_p]   0.14 GB/s  [a_p=b]   0.14 GB/s written
Case p[i]=i, 32-shuffled  -->  K=     251  [a=b_p]   0.56 GB/s  [a_p=b]   0.56 GB/s written
Case p[i]=i, 32-shuffled  -->  K=    1001  [a=b_p]   2.23 GB/s  [a_p=b]   2.23 GB/s written
Case p[i]=i, 32-shuffled  -->  K=    2001  [a=b_p]   4.46 GB/s  [a_p=b]   4.46 GB/s written
Case p[i]=i, 32-shuffled  -->  K=    5001  [a=b_p]  11.07 GB/s  [a_p=b]  11.04 GB/s written
Case p[i]=i, 32-shuffled  -->  K=   10001  [a=b_p]  21.98 GB/s  [a_p=b]  21.83 GB/s written
Case p[i]=i, 32-shuffled  -->  K=   25001  [a=b_p]  52.45 GB/s  [a_p=b]  51.99 GB/s written
Case p[i]=i, 32-shuffled  -->  K=   50001  [a=b_p] 102.36 GB/s  [a_p=b] 101.48 GB/s written
Case p[i]=i, 32-shuffled  -->  K=  100001  [a=b_p] 177.13 GB/s  [a_p=b] 174.85 GB/s written
Case p[i]=i, 32-shuffled  -->  K=  250001  [a=b_p] 174.98 GB/s  [a_p=b] 185.12 GB/s written
Case p[i]=i, 32-shuffled  -->  K=  500001  [a=b_p] 188.36 GB/s  [a_p=b] 192.49 GB/s written
Case p[i]=i, 32-shuffled  -->  K= 1000001  [a=b_p] 149.73 GB/s  [a_p=b] 154.16 GB/s written
Case p[i]=i, 32-shuffled  -->  K= 5000001  [a=b_p]  34.71 GB/s  [a_p=b]  36.06 GB/s written
Case p[i]=i, 32-shuffled  -->  K=20000001  [a=b_p]  13.67 GB/s  [a_p=b]  13.68 GB/s written
Case p[i]=i, 32-shuffled  -->  K=50000001  [a=b_p]  10.31 GB/s  [a_p=b]  10.36 GB/s written
Case fully shuffled  -->  K=      17  [a=b_p]   0.04 GB/s  [a_p=b]   0.04 GB/s written
Case fully shuffled  -->  K=      65  [a=b_p]   0.14 GB/s  [a_p=b]   0.14 GB/s written
Case fully shuffled  -->  K=     251  [a=b_p]   0.56 GB/s  [a_p=b]   0.56 GB/s written
Case fully shuffled  -->  K=    1001  [a=b_p]   2.23 GB/s  [a_p=b]   2.23 GB/s written
Case fully shuffled  -->  K=    2001  [a=b_p]   4.43 GB/s  [a_p=b]   4.43 GB/s written
Case fully shuffled  -->  K=    5001  [a=b_p]  11.02 GB/s  [a_p=b]  10.72 GB/s written
Case fully shuffled  -->  K=   10001  [a=b_p]  21.76 GB/s  [a_p=b]  21.28 GB/s written
Case fully shuffled  -->  K=   25001  [a=b_p]  51.50 GB/s  [a_p=b]  50.38 GB/s written
Case fully shuffled  -->  K=   50001  [a=b_p]  89.44 GB/s  [a_p=b]  78.70 GB/s written
Case fully shuffled  -->  K=  100001  [a=b_p] 122.87 GB/s  [a_p=b]  94.65 GB/s written
Case fully shuffled  -->  K=  250001  [a=b_p] 102.49 GB/s  [a_p=b] 100.23 GB/s written
Case fully shuffled  -->  K=  500001  [a=b_p]  78.50 GB/s  [a_p=b]  65.13 GB/s written
Case fully shuffled  -->  K= 1000001  [a=b_p]  46.09 GB/s  [a_p=b]  32.63 GB/s written
Case fully shuffled  -->  K= 5000001  [a=b_p]  21.70 GB/s  [a_p=b]  15.07 GB/s written
Case fully shuffled  -->  K=20000001  [a=b_p]  12.44 GB/s  [a_p=b]  11.22 GB/s written
Case fully shuffled  -->  K=50000001  [a=b_p]   9.98 GB/s  [a_p=b]   9.68 GB/s written
a+b  1x ->  0.0 GFLOP/s  100x ->   0.0 GFLOP/s
a+b  1x ->  0.0 GFLOP/s  100x ->   0.0 GFLOP/s
a+b  1x ->  0.0 GFLOP/s  100x ->   0.1 GFLOP/s
a+b  1x ->  0.1 GFLOP/s  100x ->   0.3 GFLOP/s
a+b  1x ->  0.2 GFLOP/s  100x ->   0.5 GFLOP/s
a+b  1x ->  0.6 GFLOP/s  100x ->   1.4 GFLOP/s
a+b  1x ->  1.1 GFLOP/s  100x ->   2.7 GFLOP/s
a+b  1x ->  2.9 GFLOP/s  100x ->   6.5 GFLOP/s
a+b  1x ->  5.6 GFLOP/s  100x ->  12.6 GFLOP/s
a+b  1x -> 10.7 GFLOP/s  100x ->  21.7 GFLOP/s
a+b  1x -> 23.1 GFLOP/s  100x ->  41.4 GFLOP/s
a+b  1x -> 17.5 GFLOP/s  100x ->  20.8 GFLOP/s
a+b  1x -> 18.0 GFLOP/s  100x ->  19.3 GFLOP/s
a+b  1x ->  3.9 GFLOP/s  100x ->   3.9 GFLOP/s
a+b  1x ->  1.6 GFLOP/s  100x ->   1.6 GFLOP/s
a+b  1x ->  1.4 GFLOP/s  100x ->   1.4 GFLOP/s
