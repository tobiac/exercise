#include "utils.h"
#include <numeric>
#include <omp.h>
#include <vector>

//using ll = long long;
using ll = unsigned int;

// Compute the sum of the Leibniz series. Each thread takes care of a subset of terms.
__global__ void leibnizKernel(ll K, double *partialSums) {
    ll idx = blockIdx.x * blockDim.x + threadIdx.x;
	ll numThreads = blockDim.x * gridDim.x;
    double sum = 0.0;
	double sign;
	ll iterPerThread = (K + ll(numThreads) - 1) / ll(numThreads); 
	ll startIdx = idx * iterPerThread;
	ll endIdx = min(K, startIdx + iterPerThread);
	for(ll i = startIdx; i < endIdx; ++i){
		sign = (i%2) ? -1.0 : 1.0;
		sum += sign / (2*i + 1);
	}

    partialSums[idx] = sum;
}

/// Run the CUDA code for the given number of blocks and threads/block.
void runCUDA(ll K, int numBlocks, int threadsPerBlock) {
    int numThreads = numBlocks * threadsPerBlock;
	printf("Compute on total of %i numThreads\n", numThreads);

    // Allocate the device and host buffers.

    double *partialSumsDev;
    double *partialSumsHost;
	printf("Starting to allocate buffers\n");

    // TODO: Allocate the temporary buffers for partial sums.
	cudaMallocHost(&partialSumsHost, numThreads * sizeof(double));
	cudaMalloc(&partialSumsDev, numThreads * sizeof(double));


    // TODO: Run the kernel and benchmark execution time.
	printf("Calling kernel with <<<%i, %i>>>\n", numBlocks, threadsPerBlock);
    double dt = 1e-9 * benchmark(100, leibnizKernel, numBlocks, threadsPerBlock, true, K, partialSumsDev);
	printf("Finished kernel\n");


    // TODO: Copy the sumsDev to host and accumulate, and sum them up.
	cudaMemcpy(partialSumsHost, partialSumsDev, numThreads * sizeof(double), cudaMemcpyDeviceToHost);
    	double sum = 0.0;
	for(int i = 0; i < numThreads; ++i){
		sum += partialSumsHost[i];
	}


    double pi = 4 * sum;
    printf("CUDA blocks=%5d  threads/block=%4d  iter/thread=%5u  pi=%.12f  rel error=%.2g  Gterms/s=%.1f\n",
           numBlocks, threadsPerBlock, K / numThreads, pi, (pi - M_PI) / M_PI,
           1e-9 * K / dt);

    // TODO: Deallocate cuda buffers.
	cudaFree(partialSumsDev);
	cudaFree(partialSumsHost);
}

/// Run the OpenMP variant of the code.
void runOpenMP(ll K, int numThreads) {
    double sum = 0.0;

    // TODO: Implement the Leibniz series summation with OpenMP.
	auto t0 = std::chrono::steady_clock::now();
	omp_set_num_threads(numThreads);
#pragma omp parallel for reduction(+ : sum)
	for(ll i = 0; i < K; ++i){
		double sign = (i%2) ? -1.0 : 1.0;
		sum += sign / (2*i + 1);
	}
	auto t1 = std::chrono::steady_clock::now();
    	double dt = 1e-9*(double)std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count();  // Time per invocation in seconds.


    // TODO: Benchmark execution time.

    double pi = 4 * sum;
    printf("OpenMP threads=%d  pi=%.16g  rel error=%.2g  Gterms/s=%.1f\n",
           numThreads, pi, (pi - M_PI) / M_PI, 1e-9 * K / dt);
};


void subtask_c() {
    constexpr ll K = 2LL << 30;

    // TODO: Experiment with number of threads per block, and number of blocks
    // (i.e. number of iterations per thread).
    runCUDA(K, 512, 512);

    runOpenMP(K, 12);
}

int main() {
    subtask_c();
}
