// TODO: (OPTIONAL) Implement `benchmark` in the utils file.
#include "utils.h"
#include <omp.h>
#include <chrono>

#define N 10000

// TODO: Task 1a.1) create an empty kernel `emptyKernel`.
__global__ void emptyKernel(){
}


/// Invoke `emptyKernel` with given number of blocks and threads/block and
/// report the execution time.
void invokeEmpty(bool synchronize, int numBlocks, int threadsPerBlock) {
    // TODO: Benchmark invocation of the `emptyKernel` code with given number
    // of blocks and threads/block.
	double dt = benchmark(N, emptyKernel, numBlocks, threadsPerBlock, synchronize);
    printf("synchronize=%d blocks=%5d  threads/block=%4d  iteration=%.1f us\n",
           (int)synchronize, numBlocks, threadsPerBlock, dt*1e-3);
};

/// Run an empty parallel region with `numThreads` threads.
void emptyParallelRegion(int numThreads) {
    // TODO: Task 1a.4) Add an OpenMP parallel region with `numThreads` threads.
    {
        // With this command we prevent the compiler from optimizing away the
        // whole parallel region.
        __asm__ volatile("");
    }
    omp_set_num_threads(numThreads);
#pragma omp parallel
    {
    
    }
};

int main() {
    // Note: You don't have to follow this skeleton.

    invokeEmpty(false, 1, 1);  // Task 1a) #1
    invokeEmpty(true, 1, 1);   // Task 1a) #2
    invokeEmpty(true, 1, 32);  // Task 1a) #3
    invokeEmpty(true, 1, 1024);
    invokeEmpty(true, 32, 1024);
    invokeEmpty(true, 1024, 32);
    invokeEmpty(true, 32768, 1);
    invokeEmpty(true, 32768, 32);
    invokeEmpty(true, 32768, 1024);


    static constexpr int numThreads = 12;
    // TODO: Task 1a.4) Benchmark `emptyParallelRegion`.

	for(int i = 0; i < (0.1*N)+1; ++i){
		emptyParallelRegion(numThreads);
	}	
	
	auto t0 = std::chrono::steady_clock::now();
	for(int i = 0; i < N; ++i){
		emptyParallelRegion(numThreads);
	}
	auto t1 = std::chrono::steady_clock::now();
	
    double dt = (double)std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count();  // Time per invocation in seconds.
	printf("dt of openmp = %f\n", dt);
	
    printf("Empty OpenMP parallel region with %d threads --> %.1f us\n",
           numThreads,  dt*1e-3);
}
