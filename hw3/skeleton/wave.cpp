#include "wave.h"

//********************************************************
//YOU ONLY NEED TO MODIFY THIS FILE TO COMPLETE HOMEWORK 3
//********************************************************

void WaveEquation::pack_all() {
  //printf("Start pack_all on %i\n", rank);
  // ********************************************************************************
  // Question 2: the number of allocated packs is 6 * threads_per_dim ^ 2
  //             as there are six faces per process and each face corresponds to
  //             threads_per_dim ^ 2 threads. Each pack is associated (numbered)
  //             with a unique ID (variable pid), that is a function of the face
  //             and the thread id (variable tid). Those id's are provided.
  //             Create a parallel region in this function and modify the inputs
  //             of pack_face accordingly, so that each thread packs the data in
  //             its own subdomain. Note that if threads_per_dim = 1, the id's
  //             of each pack reduce to the numbers 0,1,2,3,4,5.
  // ********************************************************************************
  int array_of_sizes[3] = {N + 2, N + 2, N + 2};
  int nloc = N / threads_per_dim;
  int p = threads_per_dim;
  int tid = omp_get_thread_num();
  //if(tid == 1)
  	//printf("Rank %i has %i OMP threads\n", rank, omp_get_num_threads());
  int t0, t1, t2;
  thread_coordinates(tid, threads_per_dim, t0, t1, t2);

  if (t0 == p - 1) {
    // right
    int pid = 0 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
    int array_of_subsizes[3] = {1, nloc, nloc};
    int array_of_starts[3] = {N, 1+t1*nloc, 1+t2*nloc};
    pack_face(pack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
    //printf("Rank %i in unpack (t0 == p-1) pid = %i\n", rank, pid);
  }
  if (t0 == 0) {
    // left
    int pid = 1 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
    int array_of_subsizes[3] = {1, nloc, nloc};
    int array_of_starts[3] = {1, 1+t1*nloc, 1+t2*nloc};
    pack_face(pack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
    //printf("Rank %i in unpack (t0 == 0) pid = %i\n", rank, pid);
  }
  if (t1 == p - 1) {
    // top
    int pid = 2 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
    int array_of_subsizes[3] = {nloc, 1, nloc};
    int array_of_starts[3] = {1+t0*nloc, N, 1+t2*nloc};
    pack_face(pack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
    //printf("Rank %i in unpack (t1 == p-1) pid = %i\n", rank, pid);
  }
  if (t1 == 0) {
    // bottom
    int pid = 3 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
    int array_of_subsizes[3] = {nloc, 1, nloc};
    int array_of_starts[3] = {1+t0*nloc, 1, 1+t2*nloc};
    pack_face(pack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
    //printf("Rank %i in unpack (t1 == 0) pid = %i\n", rank, pid);
  }
  if (t2 == p - 1) {
    // back
    int pid = 4 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
    int array_of_subsizes[3] = {nloc, nloc, 1};
    int array_of_starts[3] = {1+t0*nloc, 1+t1*nloc, N};
    pack_face(pack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
    //printf("Rank %i in unpack (t2 == p-1) pid = %i\n", rank, pid);
  }
  if (t2 == 0) {
    // front
    int pid = 5 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
    int array_of_subsizes[3] = {nloc, nloc, 1};
    int array_of_starts[3] = {1+t0*nloc, 1+t1*nloc, 1};
    pack_face(pack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
    //printf("Rank %i in unpack (t2 == 0) pid = %i\n", rank, pid);
  }

}

void WaveEquation::unpack_all() {
  //printf("Start pack_all on %i", rank);
  // ********************************************************************************
  // Question 2: the number of allocated unpacks is 6 * threads_per_dim ^ 2
  //             as there are six faces per process and each face corresponds to
  //             threads_per_dim ^ 2 threads. Each unpack is associated
  //             (numbered) with a unique ID (variable pid), that is a function
  //             of the face and the thread id (variable tid). Those id's are
  //             provided.
  //
  //             Each unpack should correspond to the appropriate pack (they
  //             must have the same id).
  //
  //             Create a parallel region in this function and modify the inputs
  //             of unpack_face accordingly, so that each thread unpacks the
  //             data in its own subdomain. Note that if threads_per_dim = 1,
  //             the id's of each unpack reduce to the numbers 0,1,2,3,4,5.
  // ********************************************************************************

  int array_of_sizes[3] = {N + 2, N + 2, N + 2};
  int nloc = N / threads_per_dim;
  int p = threads_per_dim;
  int tid = omp_get_thread_num();
  int t0, t1, t2;
  thread_coordinates(tid, threads_per_dim, t0, t1, t2);

  if (t0 == 0) {
    int pid = 0 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
    int array_of_subsizes[3] = {1, nloc, nloc};
    int array_of_starts[3] = {0, 1+t1*nloc, 1+t2*nloc};
    unpack_face(unpack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
  }
  if (t0 == p - 1) {
    int pid = 1 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
    int array_of_subsizes[3] = {1, nloc, nloc};
    int array_of_starts[3] = {N + 1, 1+t1*nloc, 1+t2*nloc};
    unpack_face(unpack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
  }
  if (t1 == 0) {
    int pid = 2 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
    int array_of_subsizes[3] = {nloc, 1, nloc};
    int array_of_starts[3] = {1+t0*nloc, 0, 1+t2*nloc};
    unpack_face(unpack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
  }
  if (t1 == p - 1) {
    int pid = 3 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
    int array_of_subsizes[3] = {nloc, 1, nloc};
    int array_of_starts[3] = {1+t0*nloc, N + 1, 1+t2*nloc};
    unpack_face(unpack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
  }
  if (t2 == 0) {
    int pid = 4 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
    int array_of_subsizes[3] = {nloc, nloc, 1};
    int array_of_starts[3] = {1+t0*nloc, 1+t1*nloc, 0};
    unpack_face(unpack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
  }
  if (t2 == p - 1) {
    int pid = 5 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
    int array_of_subsizes[3] = {nloc, nloc, 1};
    int array_of_starts[3] = {1+t0*nloc, 1+t1*nloc, N + 1};
    unpack_face(unpack[pid], array_of_sizes, array_of_subsizes, array_of_starts);
  }

}

void WaveEquation::run(double t_end) {
  t = 0;

  // ********************************************************************************
  // Question 2: the number of allocated unpacks is 6 * threads_per_dim ^ 2
  //             as there are six faces per process and each face corresponds to
  //             threads_per_dim ^ 2 threads. You will need to modify the
  //             variable n so that each pack/unpack has the correct size (n =
  //             N^2 is correct only when a single thread is used).
  // ********************************************************************************
  int nloc = N / threads_per_dim;

  int n = nloc * nloc;
  for (int i = 0; i < 6 * threads_per_dim * threads_per_dim; i++) {
    pack[i] = new double[n];
    unpack[i] = new double[n];
  }

  double total_time = 0;

  int count = 0;
  MPI_Barrier(cart_comm);
  double time_start = MPI_Wtime();

  // ********************************************************************************
  // Question 3: The parallel region should start outside the 'while' loop.
  // ********************************************************************************
#pragma omp parallel
{

  do {

    // Question 4: a pragma omp master might make screen output better
    #pragma omp master
{
    if (count % 10 == 0) {
      if (rank == 0)
        std::cout << count << " t=" << t << "\n";
       Print(count); //saving data really slows down the code
    }
}

    pack_all();

    // ********************************************************************************
    // Question 2: multiple threads send and receive messages, according to their
    //             thread id and 'thread coordinates' (t0,t1,t2).
    //             Be careful to correctly match the message tags pid_send and
    //             pid_recv that also correspond to the pack/unpack arrays.
    // ********************************************************************************
    int nloc = N / threads_per_dim;
    int p = threads_per_dim;
    int tid = omp_get_thread_num();
    int t0, t1, t2;
    thread_coordinates(tid, threads_per_dim, t0, t1, t2);
    int pid_send, pid_recv;

    int req_ctr = 0;

    std::vector<MPI_Request> local_request;
    if (t0 == 0) {
      local_request.resize(local_request.size() + 2);
      pid_recv = 0 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
      pid_send = 1 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
	MPI_Irecv(unpack[pid_recv], nloc*nloc, MPI_DOUBLE, rank_plus[0], pid_recv, cart_comm, &local_request[req_ctr++]);
	MPI_Isend(pack[pid_send], nloc*nloc,  MPI_DOUBLE, rank_plus[0], pid_send, cart_comm, &local_request[req_ctr++]);
	
    }
    if (t0 == p - 1) {
      local_request.resize(local_request.size() + 2);
      pid_recv = 1 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
      pid_send = 0 * threads_per_dim * threads_per_dim + t1 * threads_per_dim + t2;
	MPI_Irecv(unpack[pid_recv], nloc*nloc, MPI_DOUBLE, rank_minus[0], pid_recv, cart_comm, &local_request[req_ctr++]);
	MPI_Isend(pack[pid_send], nloc*nloc, MPI_DOUBLE, rank_minus[0], pid_send, cart_comm, &local_request[req_ctr++]);
	
    }
    if (t1 == 0) {
      local_request.resize(local_request.size() + 2);
    	pid_recv = 2 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
    	pid_send = 3 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
	MPI_Irecv(unpack[pid_recv], nloc*nloc, MPI_DOUBLE, rank_plus[1], pid_recv, cart_comm, &local_request[req_ctr++]);
	MPI_Isend(pack[pid_send], nloc*nloc, MPI_DOUBLE, rank_plus[1], pid_send, cart_comm, &local_request[req_ctr++]);

    }
    if (t1 == p - 1) {
      local_request.resize(local_request.size() + 2);
    	pid_recv = 3 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
    	pid_send = 2 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t2;
	MPI_Irecv(unpack[pid_recv], nloc*nloc, MPI_DOUBLE, rank_minus[1], pid_recv, cart_comm, &local_request[req_ctr++]);
	MPI_Isend(pack[pid_send], nloc*nloc, MPI_DOUBLE, rank_minus[1], pid_send, cart_comm, &local_request[req_ctr++]);
    }
    if (t2 == 0) {
      local_request.resize(local_request.size() + 2);
    	pid_recv = 4 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
    	pid_send = 5 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
	MPI_Irecv(unpack[pid_recv], nloc*nloc, MPI_DOUBLE, rank_plus[2], pid_recv, cart_comm, &local_request[req_ctr++]);
	MPI_Isend(pack[pid_send], nloc*nloc, MPI_DOUBLE, rank_plus[2], pid_send, cart_comm, &local_request[req_ctr++]);
    }
    if (t2 == p - 1) {
      local_request.resize(local_request.size() + 2);
    	pid_recv = 5 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
    	pid_send = 4 * threads_per_dim * threads_per_dim + t0 * threads_per_dim + t1;
	MPI_Irecv(unpack[pid_recv], nloc*nloc, MPI_DOUBLE, rank_minus[2], pid_recv, cart_comm, &local_request[req_ctr++]);
	MPI_Isend(pack[pid_send], nloc*nloc, MPI_DOUBLE, rank_minus[2], pid_send, cart_comm, &local_request[req_ctr++]);
    }
    
    // Overlap computation and communication
    int i0_min = t0 * nloc;
    int i1_min = t1 * nloc;
    int i2_min = t2 * nloc;
    for (int i0 = 1 + i0_min + 1; i0 < i0_min + nloc; i0++)
      for (int i1 = 1 + i1_min + 1; i1 < i1_min + nloc; i1++)
        for (int i2 = 1 + i2_min + 1; i2 < i2_min + nloc; i2++)
          UpdateGridPoint(i0, i1, i2);

     MPI_Waitall(local_request.size(),
     local_request.data(),MPI_STATUSES_IGNORE);

    unpack_all();
    // ********************************************************************************
    // Question 1: parallelize this loop with OPENMP, similarly to the loop
    // found in
    //             auxiliary.cpp in the WaveEquation struct constructor.
    // ********************************************************************************
    int i0, i1, i2;
    i2 = 1+i2_min;
    for (int i0 = 1 + i0_min; i0 < i0_min + nloc + 1; i0++)
      for (int i1 = 1 + i1_min; i1 < i1_min + nloc + 1; i1++)
          UpdateGridPoint(i0, i1, i2);

    i2 = i2_min+nloc;
    for (int i0 = 1 + i0_min; i0 < i0_min + nloc + 1; i0++)
      for (int i1 = 1 + i1_min; i1 < i1_min + nloc + 1; i1++)
          UpdateGridPoint(i0, i1, i2);

    i1 = 1+i1_min;
    for (int i0 = 1 + i0_min; i0 < i0_min + nloc + 1; i0++)
      for (int i2 = 1 + i2_min+1; i2 < i2_min + nloc; i2++)
          UpdateGridPoint(i0, i1, i2);
    
    i1 = i1_min+nloc;
    for (int i0 = 1 + i0_min; i0 < i0_min + nloc + 1; i0++)
      for (int i2 = 1 + i2_min+1; i2 < i2_min + nloc; i2++)
          UpdateGridPoint(i0, i1, i2);

    i0 = 1+i0_min;
    for (int i1 = 1 + i1_min+1; i1 < i1_min + nloc; i1++)
      for (int i2 = 1 + i2_min+1; i2 < i2_min + nloc; i2++)
          UpdateGridPoint(i0, i1, i2);
    
    i0 = i0_min+nloc;
    for (int i1 = 1 + i1_min+1; i1 < i1_min + nloc; i1++)
      for (int i2 = 1 + i2_min+1; i2 < i2_min + nloc; i2++)
          UpdateGridPoint(i0, i1, i2);

    // ********************************************************************************

    // ********************************************************************************
    // Question 3: You will need to add the following barrier (why?)
    // ********************************************************************************
    #pragma omp barrier

    #pragma omp master
{
    std::swap(u_new, u);
    std::swap(u_new, u_old);
    t += dt;
    count++;
}
    #pragma omp barrier
  } while (t < t_end);

}//pragma omp parallel

  MPI_Barrier(cart_comm);
  total_time = MPI_Wtime() - time_start;

  double total_time_max;
  MPI_Reduce(&total_time, &total_time_max, 1, MPI_DOUBLE, MPI_MAX, 0,
             cart_comm);
  if (rank == 0) {
    std::cout << "Total time = " << total_time_max << "\n";
  }
  double s = 0;
  double Checksum = 0;
  for (int k = 1; k <= N; k++)
    for (int j = 1; j <= N; j++)
      for (int i = 1; i <= N; i++) {
        int m = k + j * (N + 2) + i * (N + 2) * (N + 2);
        s += u[m] * u[m];
      }

  MPI_Reduce(&s, &Checksum, 1, MPI_DOUBLE, MPI_SUM, 0, cart_comm);
  if (rank == 0)
    std::cout << "Checksum = " << Checksum << "\n";

  for (int i = 6 * threads_per_dim * threads_per_dim - 1; i >= 0; i--) {
    delete[] pack[i];
    delete[] unpack[i];
  }
  delete[] pack;
  delete[] unpack;
}
