#ifndef _DIRECT_HPP_
#define _DIRECT_HPP_

#include "korali.hpp"
#include "SSA_CPU.hpp"

void direct(korali::Sample& k)
{
  double k1 = k["Parameters"][0];
  double k2 = k["Parameters"][1];
  double k3 = k["Parameters"][2];
  double k4 = k["Parameters"][3];

  // TODO: TASK 2b)
  //    - Initialize SSA_CPU class
  //    - set the rates k1, k2, k3, and k4
  //    - run
  //    - get S1 and S2
  //    - calculate objective function
  int omega = 1;
  int num_samples = 2000;
  double T = 5.0;
  double bin_dt = 0.1;

  SSA_CPU ssa(omega, num_samples, T, bin_dt);
  ssa.setRates(k1, k2, k3, k4);
  ssa();
  double s1 = ssa.getS1();
  double s2 = ssa.getS2();
  double s1Comp = s1 - 15.0;	// S1 component
  double s2Comp = s2 - 5.0;	// S2 component
  //double sse = - s1Comp*s1Comp - s2Comp*S2Comp; // TODO

  k["F(x)"] = - s1Comp*s1Comp - s2Comp*s2Comp;
}

#endif
