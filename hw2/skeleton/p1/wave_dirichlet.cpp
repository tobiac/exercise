#include "wave.h"

/********************************************************************/ 
/* Subquestion a: change the following function and use a Cartesian */ 
/* topology to find coords[3], rank_plus[3] and rank_minus[3]       */
/********************************************************************/
void WaveEquation::FindCoordinates() {

  int p = procs_per_dim;

  int dims[3] = {0,0};
  MPI_Dims_create(p*p*p, 3, dims);
  int periodic[3] = {false, false, false};

  MPI_Cart_create(MPI_COMM_WORLD, 3, dims, periodic, true, &cart_comm);
  MPI_Comm_rank(cart_comm, &rank);

  // Getting coordinate of this rank in communicator
  MPI_Cart_coords(cart_comm, rank, 3, coords);
  // Computing ranks next to this rank
  for(int i = 0; i < 3; ++i){
    MPI_Cart_shift(cart_comm, i, 1, &rank_minus[i], &rank_plus[i]);
   }
}

/********************************************************************/ 
/* Subquestion b: you should no longer need the functions pack_face */
/* and unpack_face nor should you need to allocate memory by using  */
/* double *pack[6] and double *unpack[6].                           */
/********************************************************************/
void WaveEquation::run(double t_end) {

  t = 0;

  /********************************************************************/ 
  /* Subquestion b: you need to define 12 custom datatypes.           */
  /* For sending data, six datatypes (one per face) are required.     */
  /* For receiving data, six more datatypes are required.             */
  /* You should use MPI_Type_create_subarray for those datatypes.     */
  /********************************************************************/


  /* Subquestion b: Create and commit custom datatypes here */
  /************************************************************************************************/
  MPI_Datatype SEND_FACE_PLUS [3];
  MPI_Datatype SEND_FACE_MINUS[3]; 

  MPI_Datatype RECV_FACE_PLUS [3];
  MPI_Datatype RECV_FACE_MINUS[3];

  int sizes_glob[3] = {N+2, N+2, N+2};
  int ndims = 3;
  int order = MPI_ORDER_C;
  // Create sendtypes (that's why the 'starts'-arrays start at 1)
    // left
  {
    int subsizes[3] = {1,N,N};
    int starts[3] = {1,1,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &SEND_FACE_MINUS[0]);
  }
  // right
  {
    int subsizes[3] = {1,N,N};
    int starts[3] = {N,1,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &SEND_FACE_PLUS[0]);
  }
  // bottom
  {
    int subsizes[3] = {N,1,N};
    int starts[3] = {1,1,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &SEND_FACE_MINUS[1]);
  }
  // top
  {
    int subsizes[3] = {N,1,N};
    int starts[3] = {1,N,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &SEND_FACE_PLUS[1]);
  }
  // front
  {
    int subsizes[3] = {N,N,1};
    int starts[3] = {1,1,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &SEND_FACE_MINUS[2]);
  }
  // back
  {
    int subsizes[3] = {N,N,1};
    int starts[3] = {1,1,N};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &SEND_FACE_PLUS[2]);
  }


  // Create recvtypes (that's why the 'starts'-arrays start at 0)
    // left
  {
    int subsizes[3] = {1,N,N};
    int starts[3] = {0,1,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &RECV_FACE_MINUS[0]);
  }
  // right
  {
    int subsizes[3] = {1,N,N};
    int starts[3] = {N+1,1,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &RECV_FACE_PLUS[0]);
  }
  // bottom
  {
    int subsizes[3] = {N,1,N};
    int starts[3] = {1,0,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &RECV_FACE_MINUS[1]);
  }
  // top
  {
    int subsizes[3] = {N,1,N};
    int starts[3] = {1,N+1,1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &RECV_FACE_PLUS[1]);
  }
  // front
  {
    int subsizes[3] = {N,N,1};
    int starts[3] = {1,1,0};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &RECV_FACE_MINUS[2]);
  }
  // back
  {
    int subsizes[3] = {N,N,1};
    int starts[3] = {1,1,N+1};
    MPI_Type_create_subarray(ndims, sizes_glob, subsizes, starts, order, MPI_DOUBLE, &RECV_FACE_PLUS[2]);
  }

  for(int i = 0; i < 3; ++i){
    MPI_Type_commit(&SEND_FACE_PLUS[i]);
    MPI_Type_commit(&SEND_FACE_MINUS[i]);
    MPI_Type_commit(&RECV_FACE_PLUS[i]);
    MPI_Type_commit(&RECV_FACE_MINUS[i]);
  }
 
  /************************************************************************************************/

  int count = 0;
  do {
    if (count % 5 == 0) {
      if (rank == 0)
        std::cout << count << " t=" << t << "\n";
      Print(count);
    }



    MPI_Request request[12];
	  MPI_Barrier(cart_comm);

    /* Subquestion b: Replace the sends and receives with ones that correspond to custom datatypes*/
    /**********************************************************************************************/
    for(int i = 0; i < 3; ++i){
      MPI_Irecv(u, 1, RECV_FACE_MINUS[i], rank_minus[i], (i+1)*100, cart_comm ,&request[4*i]);
      MPI_Isend(u, 1, SEND_FACE_PLUS[i], rank_plus[i], (i+1)*100, cart_comm, &request[4*i+1]);

      MPI_Irecv(u, 1, RECV_FACE_PLUS[i], rank_plus[i], (i+1)*100+1, cart_comm, &request[4*i+2]);
      MPI_Isend(u, 1, SEND_FACE_MINUS[i], rank_minus[i], (i+1)*100+1, cart_comm, &request[4*i+3]);
    }

    // Wait for communication to finish
    MPI_Waitall(12, &request[0], MPI_STATUSES_IGNORE);


    for (int i0 = 1; i0 <= N; i0++)
      for (int i1 = 1; i1 <= N; i1++)
        for (int i2 = 1; i2 <= N; i2++)
          UpdateGridPoint(i0, i1, i2);

    double *temp = u_old;
    u_old = u;
    u = u_new;
    u_new = temp;
    t += dt;
    count++;
  } while (t < t_end);

  double s = 0;
  double Checksum = 0;
  for (int k = 1; k <= N; k++)
    for (int j = 1; j <= N; j++)
      for (int i = 1; i <= N; i++) {
        int m = k + j * (N + 2) + i * (N + 2) * (N + 2);
        s += u[m] * u[m];
      }

  MPI_Reduce(&s, &Checksum, 1, MPI_DOUBLE, MPI_SUM, 0, cart_comm);
  if (rank == 0)
    std::cout << "Checksum = " << Checksum << "\n";

  /* Subquestion b: You should free the custom datatypes and the communicator here. */

  for(int i = 0; i < 3; ++i){
    MPI_Type_free(&SEND_FACE_PLUS[i]);
    MPI_Type_free(&SEND_FACE_MINUS[i]);
    MPI_Type_free(&RECV_FACE_PLUS[i]);
    MPI_Type_free(&RECV_FACE_MINUS[i]);
  }
  
    MPI_Comm_free(&cart_comm);
  
}
